#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry
{
    char * pass;
    char * hash;
};

// TODO - done
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    
    
    struct stat info;
    if(stat(filename, &info) == -1)
    {
        printf("Can't stat file\n");
        exit(1);
    }
    
    int filesize = info.st_size;
    printf("File is %d bytes\n", filesize);
    
    //allocate memory
    char *contents = malloc(filesize+1);
    
    FILE *dictionary = fopen(filename ,"rb");
    if (!dictionary)
    {
        printf("Can't open %s for reading\n", filename);
        exit(1);
    }
    
    fread(contents, 1, filesize, dictionary);
    fclose(dictionary);
    
    //add null to the end
    contents[filesize] = '\0';
    
    int lineCount = 0;
    for( int i = 0; i < filesize; i++)
    {
        if(contents[i] == '\n') lineCount++;
    }
    
    printf("Line Count: %d\n", lineCount);
    
    struct entry * rtrn;
    rtrn = (struct entry*) malloc(lineCount * sizeof(struct entry));
    
    //put first word in struct
    rtrn[0].pass = strtok(contents, "\n");
    //hash first word and put in struct
    rtrn[0].hash = md5(rtrn[0].pass, strlen(rtrn[0].pass));
    
    
    //printf("%s = %s\n", rtrn[0].pass, rtrn[0].hash);
    int i = 1;
    //put the rest of pass and hash into struct
    while((rtrn[i].pass = strtok(NULL, "\n"))!= NULL)
    {
        rtrn[i].hash = md5(rtrn[i].pass, strlen(rtrn[i].pass));
        //printf("%s = %s\n", rtrn[i].pass, rtrn[i].hash);
        free(rtrn[i].hash);
        i++;
    }
    free(rtrn[0].hash);
    *size = lineCount;
    free(contents);
    return rtrn;
}

int comparator( const void *a ,const void *b)
{
   return strcmp((char*)a , (char*)b);
  
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    //qsort(dict->hash, dlen, sizeof(char *), comparator);
    for(int i = 0; i < dlen; i++)
    {
        printf("%d %s \n",i, dict[i].hash);
    }
    // TODO
    // Open the hash file for reading.
    FILE *hashf = fopen(argv[1], "rb");
    if(!hashf)
    {
        printf("Can not open %s for reading", argv[1]);
        exit(1);
    }
    
    free(dict);
    fclose(hashf);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
}
